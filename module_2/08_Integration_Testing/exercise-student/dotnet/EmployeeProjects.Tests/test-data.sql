DELETE FROM timesheet;
DELETE FROM project_employee;
DELETE FROM employee;
DELETE FROM department;
DELETE FROM project;

-- Need a department so we can add employees
SET IDENTITY_INSERT department ON;
INSERT INTO department (department_id, name)
VALUES (1, 'Department 1');
SET IDENTITY_INSERT department OFF;

-- Need projects so we can add timesheets
SET IDENTITY_INSERT project ON;
INSERT INTO project (project_id, name, from_date, to_date)
VALUES (1, 'Project 1', '2000-01-02', '2000-12-31'),
       (2, 'Project 2', '2001-01-02', '2001-12-31');
SET IDENTITY_INSERT project OFF;

-- Need employees so we can add timesheets
SET IDENTITY_INSERT employee ON;
INSERT INTO employee (employee_id, department_id, first_name, last_name, birth_date, hire_date)
VALUES (1, 1, 'First1', 'Last1', '1981-01-01', '2001-01-02'),
       (2, 1, 'First2', 'Last2', '1982-02-01', '2002-02-03');
SET IDENTITY_INSERT employee OFF;

SET IDENTITY_INSERT timesheet ON;
INSERT INTO timesheet (timesheet_id, employee_id, project_id, date_worked, hours_worked, is_billable, description)
VALUES (1, 1, 1, '2021-01-01', 1.0, 1, 'Timesheet 1'),
       (2, 1, 1, '2021-01-02', 1.5, 1, 'Timesheet 2'),
       (3, 2, 1, '2021-01-01', 0.25, 1, 'Timesheet 3'),
       (4, 2, 2, '2021-02-01', 2.0, 0, 'Timesheet 4');
SET IDENTITY_INSERT timesheet OFF;

DBCC CHECKIDENT('timesheet', reseed, 10); -- make sure there's no chance new records will have conflicting ids
