# Module 2 - Client-Server Programming

The first two weeks of this module introduce SQL as a query language and then leverages C# and Java to issue SQL commands against databases.

The third and fourth weeks cover writing client-server applications using Web Services. This includes both implementing web service clients using either Java or C#, as well as writing server-side code that implements a RESTful web service API.

### Week 1
| Lecture | Java Quiz | .NET Quiz |
| ------- | --------- | --------- |
| [Introduction to Databases and SELECT](01_Introduction_to_Databases/lecture-notes/intro-to-databases-and-select-lecture.md) | SOC-21301945 | SOC-21295937 |
| [Ordering, Grouping, and database functions](02_Aggregate_Functions_and_GROUP_BY/lecture-notes/ordering-grouping-database-functions.md) | SOC-51709147 | SOC-51709147 |
| [Keys, JOIN, and UNION](03_Joins/lecture-notes/keys-join-union.md) | SOC-21360201 | SOC-21359486 |
| [INSERT, UPDATE, DELETE, Transactions, Constraints, and Referential Integrity](04_INSERT_UPDATE_and_DELETE/lecture-notes/insert-update-delete-transactions-constraints.md) | SOC-21387216 | SOC-21393748 |
| **Week 1 Review** | N/A | N/A |

### Week 2
| Lecture | Java Quiz | .NET Quiz |
| ------- | --------- | --------- |
| [Database design, CREATE DATABASE, CREATE USER, CREATE TABLE, ALTER TABLE](06_Database_Design/lecture-notes/database-design.md) | N/A | N/A |
| [Database Connectivity and DAO](07_Database_Connectivity_DAO/lecture-notes/database-connectivity.md) | SOC-31135631 | SOC-21503664 |
| [Integration Testing](08_Integration_Testing/lecture-notes/integration-testing.md) | N/A | N/A |
| [Data Security](09_Data_Security) | N/A | N/A |
| **Week 2 Review** | N/A | N/A |

### Week 3
| Lecture | Java Quiz | .NET Quiz | HTTP Quiz (Day 1 only) |
|---------|-----------|-----------| ---------------------- |
| [HTTP and Consuming APIs (Part 1)](11_HTTP_Web_Services_GET/lecture-notes/README.md) | SOC-51903292 | SOC-48494084 | SOC-48494085 |
| [Consuming APIs (Part 2)](12_HTTP_Web_Services_POST/lecture-notes/README.md) | SOC-48494488 | SOC-48461418 |
| [Server-Side APIs (Part 1)](13_ServerSide_APIs_Part_1/lecture-notes/README.md) | SOC-48494649 | SOC-48494651 |
| [Server-Side APIs (Part 2)](14_ServerSide_APIs_Part_2/lecture-notes/README.md) | SOC-48474621 | SOC-48474829 |
| **Week 3 Review** | N/A | N/A |

### Week 4
| Lecture | Java Quiz | .NET Quiz |
|---------|-----------|-----------|
| [Authentication](16_Authentication/lecture-notes/README.md) | SOC-48495747 | SOC-48475190 |
| Capstone Day 1 | N/A | N/A |
| Capstone Day 2 | N/A | N/A |
| Capstone Day 3 | N/A | N/A |
